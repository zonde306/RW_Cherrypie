﻿using HarmonyLib;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Cherrypie
{
	[StaticConstructorOnStartup]
	public static class RJW_ApplyHymen
	{
		static RJW_ApplyHymen()
		{
			var harrmoy = new Harmony("rjw.Cherrypie");
			harrmoy.Patch(
				AccessTools.Method(typeof(SexPartAdder), nameof(SexPartAdder.add_genitals)),
				prefix: null, postfix: new HarmonyMethod(AccessTools.Method(typeof(RJW_ApplyHymen), nameof(RJW_ApplyHymen.Postfix))));
		}

		public static void Postfix(Pawn pawn)
		{
			Hediff vagina = pawn.GetGenitalsList().Find(Genital_Helper.is_vagina);
			if (vagina != null)
				pawn.health.AddHediff(DefineOf.Hymen, vagina.Part);
		}
	}
}

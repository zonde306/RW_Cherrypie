﻿using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Cherrypie
{
	[HarmonyPatch(typeof(JobDriver_SexBaseInitiator), "Start")]
	public static class RJW_PoptheCherry
	{
		static private HashSet<string> Lv5Races;
		static private HashSet<string> Lv4Races;
		static private HashSet<string> Lv3Races;

		static RJW_PoptheCherry()
		{
			Lv5Races = new HashSet<string>{
				"Momona",
				"Nemuri",
				"Liuli",
				"Nonona",
				"Ulrica",
				"Ariana",
				"Alphazero",
			};

			Lv4Races = new HashSet<string> {
				"akame",
				"aome",
				"hana",
				"kohime",
				"kon",
				"konf",
				"kuro",
				"miko",
				"siro",
				"sirof",
				"yuki",
				"Maidnukos",
				"baier",
				"buer",
				"jiaer",
				"juer",
				"kuoer",
				"zhuer",
			};

			Lv3Races = new HashSet<string> {
				"azinn",
				"BTgo",
				"doxuvuxa",
				"ISwann",
				"karuro",
				"karuroBunny",
				"mausu",
				"mausuBunny",
				"mausumeido",
				"mausumizugi",
				"mausutyaina",
				"sanngou",
				"Tnanazyuu",
				"Tniiroku",
				"Tsannyonn",
				"VAA_v_flower",
				"VAA_YuzukiYukari",
				"VAA_KizunaAkari",
				"VAA_NineTailBunshin",
				"VAA_Tuina_chan",
				"VAA_MeikaHime",
				"VAA_MeikaMikoto",
				"VAA_TohokuZunko",
				"VAA_TohokuKiritan",
				"VAA_KotonohaAkane",
				"VAA_KotonohaAoi",
				"VAA_SatoSasara",
				"VAA_SuzukiTsudumi",
				"VAA_IA",
				"VAA_ONE",
				"VAA_TohokuItako",
				"VAA_OtomachiUna",
			};
		}

		[HarmonyPostfix]
		public static void Postfix(JobDriver_SexBaseInitiator __instance)
		{
			if (__instance.Partner == null || __instance.Sexprops?.sexType != xxx.rjwSextype.Vaginal)
				return;

			if (__instance.Partner.gender == Gender.Female && __instance.pawn.gender == Gender.Male)
				Defloration(__instance.Partner, __instance.pawn, __instance.Sexprops?.isRape ?? false);
			else if (__instance.pawn.gender == Gender.Female && __instance.Partner.gender == Gender.Male)
				Defloration(__instance.pawn, __instance.Partner, __instance.Sexprops?.isRape ?? false);
			else
				Log.Warning($"[Cherrypie] Unknown attacker {__instance.Partner.Name.ToStringShort}({__instance.Partner.gender}) and {__instance.pawn.Name.ToStringShort}({__instance.pawn.gender})");
		}

		private static void Defloration(Pawn female, Pawn male, bool rape)
		{
			List<Hediff> hymens = female.health.hediffSet.hediffs.FindAll(hed => hed.def.defName == DefineOf.Hymen.defName);
			if (hymens.NullOrEmpty())
			{
				// Log.Message($"[Cherrypie] {female.Name.ToStringShort} not {DefineOf.Hymen.defName}.");
				return;
			}
			
			hymens.ForEach(female.health.RemoveHediff);

			Hediff penis = male.GetPenis();
			if (penis == null)
			{
				Log.Message($"[Cherrypie] {male.Name.ToStringShort} not penis.");
				return;
			}

			Hediff vagina = female.GetVagina();
			if (vagina == null)
			{
				Log.Message($"[Cherrypie] {female.Name.ToStringShort} not vagina.");
				return;
			}

			float damage = penis.GetSize() / vagina.GetSize();
			if (damage > 1.25f)
				damage *= 1.5f;
			if (rape)
				damage *= 1.5f;

			DeflorationPrefix(female, male, rape, penis, vagina, ref damage);
			Log.Message($"[Cherrypie] {female.Name.ToStringShort} defloration by {male.Name.ToStringShort} damage {damage}");

			if (female.TryGetComp<RecordComp>()?.IsDefloration != true)
			{
				// female.records.AddTo(DefineOf.DefloraTime, female.ageTracker.AgeBiologicalYears);
				// female.records.AddTo(DefineOf.Deflower, male.thingIDNumber);
				female.TryGetComp<RecordComp>()?.Crown(male, female.ageTracker.AgeBiologicalYears, Find.TickManager.TicksGame);
				Messages.Message($"{female.Name.ToStringShort} 被 {male.Name.ToStringShort} 吃掉了", female, MessageTypeDefOf.PositiveEvent, true);
			}
			if(male.TryGetComp<RecordComp>()?.IsDefloration != true)
			{
				male.TryGetComp<RecordComp>()?.Crown(female, male.ageTracker.AgeBiologicalYears, Find.TickManager.TicksGame);
			}

			if (damage < .5f)
				return;

			Hediff hediff = HediffMaker.MakeHediff(DefineOf.DeflorationDamage.hediff, female, vagina.Part);
			hediff.Severity = damage;
			DamageInfo damageInfo = new DamageInfo(DefineOf.DeflorationDamage, damage, instigator: male, hitPart: vagina.Part);
			damageInfo.SetIgnoreArmor(true);
			female.health.AddHediff(hediff, vagina.Part, damageInfo);

			DeflorationPostfix(female, male, rape, penis, vagina, damage);
		}

		public static void DeflorationPrefix(Pawn female, Pawn male, bool rape, Hediff penis, Hediff vagina, ref float damage)
		{
			damage = Mathf.Clamp(damage, 0f, female.health.hediffSet.GetPartHealth(vagina.Part) / 2);

			int level = 0;
			if (male.needs != null)
			{
				if (male.needs.mood?.thoughts?.memories != null)
				{
					float beauty = female.GetStatValue(StatDefOf.PawnBeauty, true);
					if (Lv5Races.Contains(female.def.defName))
						level = 4;
					else if (Lv4Races.Contains(female.def.defName))
						level = 3;
					else if (beauty >= 2 || Lv3Races.Contains(female.def.defName))
						level = 2;
					else if (beauty > 0 || xxx.is_animal(female))
						level = 1;

					male.needs.mood.thoughts.memories.TryGainMemory(ThoughtMaker.MakeThought(DefineOf.CherrypieMale, level), female);
				}

				if (level >= 3 && male.needs.rest != null)
					male.needs.rest.CurLevel = male.needs.rest.MaxLevel;
				if (level >= 3 && male.needs.food != null)
					male.needs.food.CurLevel = male.needs.food.MaxLevel;
				if (level >= 2 && male.needs.joy != null)
					male.needs.joy.CurLevel = male.needs.joy.MaxLevel;
			}

			if (level >= 4)
				CureBadHediffHelper.DoEffect(male);

			Log.Message($"[Cherrypie] {male.Name.ToStringShort} level {level}");
		}

		public static void DeflorationPostfix(Pawn female, Pawn male, bool rape, Hediff penis, Hediff vagina, float damage)
		{
			int level = 1;
			if (female.needs?.mood?.thoughts?.memories != null)
			{
				int relation = female.relations.OpinionOf(male);
				if (female.relations.DirectRelationExists(PawnRelationDefOf.Spouse, male) ||
					female.relations.DirectRelationExists(PawnRelationDefOf.Lover, male))
					level = 4;
				else if (relation >= 80)
					level = 3;
				else if (relation >= 40)
					level = 2;
				else if(rape || relation <= 0)
					level = 0;

				female.needs.mood.thoughts.memories.TryGainMemory(ThoughtMaker.MakeThought(DefineOf.CherrypieFemale, level), male);
			}
		}

		public static Hediff GetPenis(this Pawn pawn) => pawn.health.hediffSet.hediffs.Find(Genital_Helper.is_penis);

		public static Hediff GetVagina(this Pawn pawn) => pawn.health.hediffSet.hediffs.Find(Genital_Helper.is_vagina);

		public static float GetSize(this Hediff organ) => organ.Severity * organ.pawn.BodySize;
	}
}

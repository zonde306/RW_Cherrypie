﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Cherrypie
{
	public static class CureBadHediffHelper
	{
		// Token: 0x0600000A RID: 10 RVA: 0x00002220 File Offset: 0x00000420
		public static void DoEffect(Pawn usedBy)
		{
			for (int i = 0; i < 5; i++)
			{
				Hediff hediff = FindLifeThreateningHediff(usedBy);
				if (hediff != null)
				{
					Cure(hediff);
				}
				if (HealthUtility.TicksUntilDeathDueToBloodLoss(usedBy) < 2500)
				{
					Hediff hediff2 = FindMostBleedingHediff(usedBy);
					if (hediff2 != null)
					{
						Cure(hediff2);
					}
				}
				if (usedBy.health.hediffSet.GetBrain() != null)
				{
					Hediff_Injury hediff_Injury = FindPermanentInjury(usedBy, Gen.YieldSingle<BodyPartRecord>(usedBy.health.hediffSet.GetBrain()));
					if (hediff_Injury != null)
					{
						Cure(hediff_Injury);
					}
				}
				BodyPartRecord bodyPartRecord = FindBiggestMissingBodyPart(usedBy, ThingDefOf.Human.race.body.GetPartsWithDef(BodyPartDefOf.Hand).First<BodyPartRecord>().coverageAbsWithChildren);
				if (bodyPartRecord != null)
				{
					Cure(bodyPartRecord, usedBy);
				}
				Hediff_Injury hediff_Injury2 = FindPermanentInjury(usedBy,
					from x in usedBy.health.hediffSet.GetNotMissingParts(0, 0, null, null)
					where x.def == BodyPartDefOf.Eye select x);
				if (hediff_Injury2 != null)
				{
					Cure(hediff_Injury2);
				}
				Hediff hediff3 = FindImmunizableHediffWhichCanKill(usedBy);
				if (hediff3 != null)
				{
					Cure(hediff3);
				}
				Hediff hediff4 = FindNonInjuryMiscBadHediff(usedBy, true);
				if (hediff4 != null)
				{
					Cure(hediff4);
				}
				Hediff hediff5 = FindNonInjuryMiscBadHediff(usedBy, false);
				if (hediff5 != null)
				{
					Cure(hediff5);
				}
				if (usedBy.health.hediffSet.GetBrain() != null)
				{
					Hediff_Injury hediff_Injury3 = FindInjury(usedBy, Gen.YieldSingle<BodyPartRecord>(usedBy.health.hediffSet.GetBrain()));
					if (hediff_Injury3 != null)
					{
						Cure(hediff_Injury3);
					}
				}
				BodyPartRecord bodyPartRecord2 = FindBiggestMissingBodyPart(usedBy, 0f);
				if (bodyPartRecord2 != null)
				{
					Cure(bodyPartRecord2, usedBy);
				}
				Hediff_Addiction hediff_Addiction = FindAddiction(usedBy);
				if (hediff_Addiction != null)
				{
					Cure(hediff_Addiction);
				}
				Hediff_Injury hediff_Injury4 = FindPermanentInjury(usedBy, null);
				if (hediff_Injury4 != null)
				{
					Cure(hediff_Injury4);
				}
				Hediff_Injury hediff_Injury5 = FindInjury(usedBy, null);
				if (hediff_Injury5 != null)
				{
					Cure(hediff_Injury5);
				}
			}
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000023F4 File Offset: 0x000005F4
		private static Hediff FindLifeThreateningHediff(Pawn pawn)
		{
			Hediff hediff = null;
			float num = -1f;
			foreach (Hediff hediff2 in pawn.health.hediffSet.hediffs)
			{
				if (hediff2.Visible && hediff2.def.everCurableByItem && !HediffUtility.FullyImmune(hediff2))
				{
					HediffStage curStage = hediff2.CurStage;
					if ((curStage != null && curStage.lifeThreatening) || (hediff2.def.lethalSeverity >= 0f && hediff2.Severity / hediff2.def.lethalSeverity >= 0.8f))
					{
						BodyPartRecord part = hediff2.Part;
						float num2 = (part != null) ? part.coverageAbsWithChildren : 999f;
						if (hediff == null || num2 > num)
						{
							hediff = hediff2;
							num = num2;
						}
					}
				}
			}
			return hediff;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000024DC File Offset: 0x000006DC
		private static Hediff FindMostBleedingHediff(Pawn pawn)
		{
			float num = 0f;
			Hediff hediff = null;
			foreach (Hediff hediff2 in pawn.health.hediffSet.hediffs)
			{
				if (hediff2.Visible && hediff2.def.everCurableByItem)
				{
					float bleedRate = hediff2.BleedRate;
					if (bleedRate > 0f && (bleedRate > num || hediff == null))
					{
						num = bleedRate;
						hediff = hediff2;
					}
				}
			}
			return hediff;
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002570 File Offset: 0x00000770
		private static Hediff FindImmunizableHediffWhichCanKill(Pawn pawn)
		{
			Hediff hediff = null;
			float num = -1f;
			foreach (Hediff hediff2 in pawn.health.hediffSet.hediffs)
			{
				if (hediff2.Visible && hediff2.def.everCurableByItem && HediffUtility.TryGetComp<HediffComp_Immunizable>(hediff2) != null && !HediffUtility.FullyImmune(hediff2) && CanEverKill(hediff2))
				{
					float severity = hediff2.Severity;
					if (hediff == null || severity > num)
					{
						hediff = hediff2;
						num = severity;
					}
				}
			}
			return hediff;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002614 File Offset: 0x00000814
		private static Hediff FindNonInjuryMiscBadHediff(Pawn pawn, bool onlyIfCanKill)
		{
			Hediff hediff = null;
			float num = -1f;
			foreach (Hediff hediff2 in pawn.health.hediffSet.hediffs)
			{
				if (hediff2.Visible && hediff2.def.isBad && hediff2.def.everCurableByItem && !(hediff2 is Hediff_Injury) && !(hediff2 is Hediff_MissingPart) && !(hediff2 is Hediff_Addiction) && !(hediff2 is Hediff_AddedPart) && (!onlyIfCanKill || CanEverKill(hediff2)))
				{
					BodyPartRecord part = hediff2.Part;
					float num2 = (part != null) ? part.coverageAbsWithChildren : 999f;
					if (hediff == null || num2 > num)
					{
						hediff = hediff2;
						num = num2;
					}
				}
			}
			return hediff;
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000026EC File Offset: 0x000008EC
		private static BodyPartRecord FindBiggestMissingBodyPart(Pawn pawn, float minCoverage = 0f)
		{
			BodyPartRecord bodyPartRecord = null;
			foreach (Hediff_MissingPart hediff_MissingPart in pawn.health.hediffSet.GetMissingPartsCommonAncestors())
			{
				if (hediff_MissingPart.Part.coverageAbsWithChildren >= minCoverage && !pawn.health.hediffSet.PartOrAnyAncestorHasDirectlyAddedParts(hediff_MissingPart.Part) && (bodyPartRecord == null || hediff_MissingPart.Part.coverageAbsWithChildren > bodyPartRecord.coverageAbsWithChildren))
				{
					bodyPartRecord = hediff_MissingPart.Part;
				}
			}
			return bodyPartRecord;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002788 File Offset: 0x00000988
		private static Hediff_Addiction FindAddiction(Pawn pawn)
		{
			foreach (Hediff hediff in pawn.health.hediffSet.hediffs)
			{
				Hediff_Addiction hediff_Addiction = hediff as Hediff_Addiction;
				if (hediff_Addiction != null && hediff_Addiction.Visible && hediff_Addiction.def.everCurableByItem)
				{
					return hediff_Addiction;
				}
			}
			return null;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002804 File Offset: 0x00000A04
		private static Hediff_Injury FindPermanentInjury(Pawn pawn, IEnumerable<BodyPartRecord> allowedBodyParts = null)
		{
			Hediff_Injury hediff_Injury = null;
			foreach (Hediff hediff in pawn.health.hediffSet.hediffs)
			{
				Hediff_Injury hediff_Injury2 = hediff as Hediff_Injury;
				if (hediff_Injury2 != null && hediff_Injury2.Visible && HediffUtility.IsPermanent(hediff_Injury2) && hediff_Injury2.def.everCurableByItem && (allowedBodyParts == null || allowedBodyParts.Contains(hediff_Injury2.Part)) && (hediff_Injury == null || hediff_Injury2.Severity > hediff_Injury.Severity))
				{
					hediff_Injury = hediff_Injury2;
				}
			}
			return hediff_Injury;
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000028A8 File Offset: 0x00000AA8
		private static Hediff_Injury FindInjury(Pawn pawn, IEnumerable<BodyPartRecord> allowedBodyParts = null)
		{
			Hediff_Injury hediff_Injury = null;
			foreach (Hediff hediff in pawn.health.hediffSet.hediffs)
			{
				Hediff_Injury hediff_Injury2 = hediff as Hediff_Injury;
				if (hediff_Injury2 != null && hediff_Injury2.Visible && hediff_Injury2.def.everCurableByItem && (allowedBodyParts == null || allowedBodyParts.Contains(hediff_Injury2.Part)) && (hediff_Injury == null || hediff_Injury2.Severity > hediff_Injury.Severity))
				{
					hediff_Injury = hediff_Injury2;
				}
			}
			return hediff_Injury;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002944 File Offset: 0x00000B44
		private static void Cure(Hediff hediff)
		{
			Pawn pawn = hediff.pawn;
			pawn.health.RemoveHediff(hediff);
			if (hediff.def.cureAllAtOnceIfCuredByItem)
			{
				int num = 0;
				for (; ; )
				{
					num++;
					if (num > 10000)
					{
						break;
					}
					Hediff firstHediffOfDef = pawn.health.hediffSet.GetFirstHediffOfDef(hediff.def, false);
					if (firstHediffOfDef == null)
					{
						return;
					}
					pawn.health.RemoveHediff(firstHediffOfDef);
				}
				Log.Error("Too many iterations.");
			}
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000029E2 File Offset: 0x00000BE2
		private static void Cure(BodyPartRecord part, Pawn pawn)
		{
			pawn.health.RestorePart(part, null, true);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002A20 File Offset: 0x00000C20
		private static bool CanEverKill(Hediff hediff)
		{
			if (hediff.def.stages == null)
			{
				return hediff.def.lethalSeverity >= 0f;
			}
			using (List<HediffStage>.Enumerator enumerator = hediff.def.stages.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current.lifeThreatening)
					{
						return true;
					}
				}
			}
			return hediff.def.lethalSeverity >= 0f;
		}
	}
}

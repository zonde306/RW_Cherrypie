﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using HarmonyLib;
using System.Reflection;

namespace Cherrypie
{
    [StaticConstructorOnStartup]
    public static class Main
    {
        static Main()
        {
            var harmony = new Harmony("rjw.Cherrypie");
            harmony.PatchAll(Assembly.GetExecutingAssembly());

            var defs = DefDatabase<ThingDef>.AllDefs.Where(x => x.race != null && !x.race.IsMechanoid);
            if(!defs.EnumerableNullOrEmpty())
            {
				CompProperties comp = new CompProperties(typeof(RecordComp));
                foreach(var def in defs)
                    def.comps.Add(comp);
			}
		}
	}
}

﻿using RimWorld;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Cherrypie
{
	public class Recipe_HymenSurgery : Recipe_Surgery
	{
		public override IEnumerable<BodyPartRecord> GetPartsToApplyOn(Pawn pawn, RecipeDef recipe)
		{
			if (pawn.gender != Gender.Female)
				yield break;

			BodyPartRecord part = Genital_Helper.get_genitalsBPR(pawn);
			if (part == null)
				yield break;

			List<Hediff> hediffs = Genital_Helper.get_PartsHediffList(pawn, part);
			if (Genital_Helper.has_vagina(pawn, hediffs) && !HasHymen(pawn, hediffs))
				yield return part;
		}

		public override void ApplyOnPawn(Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients, Bill bill)
		{
			if (billDoer == null)
				return;

			pawn.health.AddHediff(DefineOf.Hymen, part);
			pawn.GetGenitalsList().FindAll(hed => hed.def.defName == DefineOf.DeflorationDamage.defName).ForEach(pawn.health.RemoveHediff);
		}

		private static bool HasHymen(Pawn pawn, List<Hediff> parts)
		{
			if (parts.NullOrEmpty())
				return false;

			return parts.Any(hed => hed.def.defName == DefineOf.Hymen.defName);
		}
	}

	[StaticConstructorOnStartup]
	public static class SurgeryRacePatch
	{
		static SurgeryRacePatch()
		{
			DefDatabase<ThingDef>.AllDefs.Where(
				def => def.race != null &&
				(def.race.Humanlike || def.race.Animal) &&
				!DefineOf.Surgery_RestoreHymen.recipeUsers.Contains(def)).ToList().ForEach(
					DefineOf.Surgery_RestoreHymen.recipeUsers.Add);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Cherrypie
{
	public class RecordComp : ThingComp
	{
		public int Age = -1;
		public int Tick = -1;
		public Pawn Crowner = null;
		public string CachedPawnName;

		public bool IsDefloration => Crowner != null || Age > -1 || Tick > -1;

		public override void PostExposeData()
		{
			Scribe_References.Look(ref Crowner, "deflcrowner");
			Scribe_Values.Look(ref Age, "deflage", -1);
			Scribe_Values.Look(ref Tick, "defltime", -1);
			Scribe_Values.Look(ref CachedPawnName, "deflname", "");

			base.PostExposeData();
		}

		public void Crown(Pawn crowner, int age, int time)
		{
			if (Crowner != null)
				return;

			Crowner = crowner;
			Age = age;
			Tick = time;
			CachedPawnName = crowner.Name.ToStringFull;
		}
	}
}

﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Cherrypie
{
	[DefOf]
	public class DefineOf
	{
		public static DamageDef DeflorationDamage;
		public static ThoughtDef CherrypieFemale;
		public static ThoughtDef CherrypieMale;
		public static HediffDef Hymen;
		public static RecordDef Deflower;
		public static RecordDef DefloraTime;
		public static RecipeDef Surgery_RestoreHymen;
	}
}

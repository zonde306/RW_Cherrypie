﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using rjw;
using UnityEngine;
using Verse;

namespace Cherrypie
{
	[StaticConstructorOnStartup]
	public class Patch_RecordCard
	{
		static Patch_RecordCard()
		{
			var harrmoy = new Harmony("rjw.Cherrypie");
			harrmoy.Patch(
				AccessTools.Method(typeof(RecordsCardUtility), "DrawRecord"),
				prefix: new HarmonyMethod(AccessTools.Method(typeof(Patch_RecordCard), nameof(Patch_RecordCard.Prefix))),
				postfix: null);
		}

		public static bool Prefix(float x, float y, float width, RecordDef record, Pawn pawn, ref float __result)
		{
			if(record.index != DefineOf.Deflower.index && record.index != DefineOf.DefloraTime.index)
				return true;

			float num = width * 0.45f;
			string text = ((record.type != 0) ? pawn.records.GetValue(record).ToString("0.##") : pawn.records.GetAsInt(record).ToStringTicksToPeriod());

			RecordComp comp = pawn.TryGetComp<RecordComp>();
			if (record.index == DefineOf.Deflower.index)
			{
				if (comp?.IsDefloration == true)
				{
					text = comp?.Crowner?.Name?.ToStringFull ?? comp?.CachedPawnName ?? "未知";
				}
				else if(pawn.gender == Gender.Female)
				{
					text = "处女";
				}
				else if(pawn.gender == Gender.Male)
				{
					text = "处男";
				}
				else
				{
					text = "处";
				}
			}
			else if(record.index == DefineOf.DefloraTime.index)
			{
				if (comp?.IsDefloration == true)
				{
					text = comp?.Age.ToString() + "岁";
				}
				else if (pawn.gender == Gender.Female)
				{
					text = "处女";
				}
				else if (pawn.gender == Gender.Male)
				{
					text = "处男";
				}
				else
				{
					text = "处";
				}
			}

			Rect rect = new Rect(8f, y, width, Text.CalcHeight(text, num));
			if (Mouse.IsOver(rect))
			{
				Widgets.DrawHighlight(rect);
			}

			Rect rect2 = rect;
			rect2.width -= num;
			Widgets.Label(rect2, record.LabelCap);
			Rect rect3 = rect;
			rect3.x = rect2.xMax;
			rect3.width = num;
			Widgets.Label(rect3, text);
			if (Mouse.IsOver(rect))
			{
				TooltipHandler.TipRegion(rect, new TipSignal(() => record.description, record.GetHashCode()));

				if (Input.GetMouseButtonDown(0) && comp?.Crowner != null && comp?.Crowner?.IsWorldPawn() == false)
					Find.Selector.Select(comp.Crowner);
			}

			__result = rect.height;

			return false;
		}
	}
}
